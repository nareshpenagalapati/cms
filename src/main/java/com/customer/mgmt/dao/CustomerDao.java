package com.customer.mgmt.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.customer.mgmt.model.Customer;

@Repository
public interface CustomerDao extends JpaRepository<Customer, Long> {

	@Override
	List<Customer> findAll();
}
