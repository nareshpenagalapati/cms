package com.customer.mgmt.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.customer.mgmt.dao.CustomerDao;
import com.customer.mgmt.exception.CustomerNotFoundException;
import com.customer.mgmt.model.Customer;

@Service
public class CustomerService {

	@Autowired
	CustomerDao customerDao;

	public Customer addCustomer(Customer customer) {
		return customerDao.save(customer);
	}

	public List<Customer> getCustomers() {
		return customerDao.findAll();
	}

	public Customer getCustomer(long customerId) {
		Optional<Customer> optionalCustomer = Optional.ofNullable(customerDao.findOne(customerId));
		if (!optionalCustomer.isPresent()) {
			throw new CustomerNotFoundException("Customer Record not found ...");
		}
		return optionalCustomer.get();
	}

	public Customer updateCustomer(long customerId, Customer customer) {
		customer.setCustomerId(customerId);
		return customerDao.save(customer);
	}

	public void deleteCustomer(long customerId) {
		customerDao.findOne(customerId);

	}

}
